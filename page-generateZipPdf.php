<?php
/**
 * Template Name: PDF Zip
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */
 if ( !is_user_logged_in() ) { wp_redirect( home_url('/index.php/ingreso/') ); exit;}
 global $current_user, $wp_roles;
// if (cuantosCVs($current_user->ID)<=0){ wp_redirect( home_url('/index.php/crear-mi-cv/') ); exit;}
if( $current_user->has_cap( 'administrator') or $current_user->has_cap( 'hr_admin')) {$isHable = true;}
else{ wp_redirect( home_url() ); exit;}

get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <?php 



$args = array(
    'role'    => 'author',
    'orderby' => 'user_nicename',
    'order'   => 'ASC'
);

$users = get_users( $args );

            if( $users ): ?>
				<div class="filename">
					<label>Zip File Name</label><input type='text' name='zipname' id='zipname'/>
				</div>
                <ul id="usr2Pdf">
                <?php foreach ( $users as $user ) : ?>
                    <li>
						<h4><input type='checkbox' class='ifShowed' value='<?php echo $user->ID;?>' names='theUsers'> <a href="#"><?php  echo esc_html( $user->display_name ); ?></a></h4><?php echo gotPapeleria($user->ID);?><?php echo getExamenLink($user->ID);?><div class="rempamer">Solicitar Completar</div>
                    </li>
                <?php endforeach; ?>
                </ul>
                <?php wp_reset_postdata(); ?>
                <div class="make-pdf">Crear PDF</div><div id="dwlink"><a href="">Descargar PDF</a></div>
            <?php endif; ?>


<script type="text/javascript">
var pdfAjaxUrl = "/wp-admin/admin-ajax.php";
hr = jQuery.noConflict();
hr(document).ready(function() {
	hr('#dwlink').fadeOut();
	hr('.make-pdf').click(function(e){
		var listArray = "";
		var order =  hr('#usr2Pdf li input');
		var zipname = hr("#zipname").val().replace(/ /g,"_");
		
		hr.each(order,function (k,v){
			if(hr(v).is(':checked')){
				listArray += hr(v).val()+",";
			}
		});
        listArray = listArray.slice(0, -1);
		hr('.make-pdf').fadeOut();
        hr('#dwlink').fadeOut();
		hr.ajax({
			url : pdfAjaxUrl,
			type : 'post',
			data : {
				action : 'hr_cv_pdf_download',
				zipname : zipname,
				order_list : listArray
			},
			success : function( response ) {
				hr('#dwlink a').html("<a href="+response+">Descargar Papelerias</a>");
                hr('#dwlink').fadeIn();
			}
		});
	});
	hr('.ifShowed').click(function(e){
		hr('.make-pdf').fadeIn();
	})
});
</script>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();?>