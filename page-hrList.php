<?php
/**
 * Template Name: Page Order PDF List
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */
//if ( is_user_logged_in() ) { wp_redirect( home_url('/index.php/crear-mi-cv/') ); exit;}
 			wp_deregister_script( 'jquery' );
/*			wp_register_script( 'jquery-new', get_template_directory_uri() . '/assets/js/jquery-1.3.2.min.js' );		
			wp_register_script( 'jquery-iu', get_template_directory_uri() . '/assets/js/jquery-ui-1.7.1.custom.min.js',array('jquery-new') );*/
/*						wp_localize_script( 'pdforder', 'pdforderjs', array(
					'ajax_url' => admin_url( 'admin-ajax.php' )
			));	*/
$filesArray = array('dpi','cv','antecedentes_penales','antecedentes_policiacos','cartas','diplomas','nit','igss','record_crediticio','recibos');
global $current_user;
$myOrder = get_user_meta($current_user->ID,'pdf_order');	
$userData = get_user_meta($current_user->ID,'pdf_images',true);			
$hayStak = explode(',',$myOrder[0]);
get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php 

			?>
			<div class="make-pdf">Establecer order de PDF</div>
			<div class="paperContainer">
				<div class="myList">
					<h3>Documentos Disponibles</h3>
				<ul id="files-list">
				<?php 
					foreach ($hayStak as $key => $value) {
							$status = "checked='checked'";
							if($value != ""){
								echo "<li id='item_$value'><img src='".get_template_directory_uri()."/assets/images/arrow.png' alt='move' width='16' height='16' class='handle' /><input type='checkbox' class='ifShowed' value='$value' $status> $value</li>";
							}
					}
				?>						
				<?php 
					foreach ($filesArray as $key => $value) {
						if((!in_array($value,$hayStak))){
							$status = "";
							echo "<li id='item_$value'><img src='".get_template_directory_uri()."/assets/images/arrow.png' alt='move' width='16' height='16' class='handle' /><input type='checkbox' class='ifShowed' value='$value' $status> $value</li>";
						}
					}
				?>
				</ul>
				</div>
				<div class="myList">
					<h3>Mi orden de Impresion</h3>
					<ul id="muList">
					<?php 
						foreach ($hayStak as $key => $value) {
							if($value != ""){
								echo "<li id='my_list_item_$value'><img src='".get_template_directory_uri()."/assets/images/arrow.png' alt='move' width='16' height='16' class='handle' /> $value</li>";
							}
						}
					?>						
					</ul>
								
				</div>
			</div>
			<?php// callFromPage(6);?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.3.2.min.js'></script>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/jquery-ui-1.7.1.custom.min.js'></script>
<script type="text/javascript">
var pdfAjaxUrl = "/wp-admin/admin-ajax.php";
var redirecURL = '//atento.creatucv.com/index.php/descargar-cvs/';
hr = jQuery.noConflict();
hr(document).ready(function() {
	hr('.make-pdf').fadeOut();
    hr("#files-list").sortable({
      handle : '.handle',
      update : function () {
		hr('.make-pdf').fadeIn();	  
      }
    });

	hr('.make-pdf').click(function(e){
		var listArray = "";
		var order =  hr('#files-list li input');
		hr.each(order,function (k,v){
			if(hr(v).is(':checked')){
				listArray += hr(v).val()+",";
			}
		});
		hr('.make-pdf').fadeOut();
		
		hr.ajax({
			url : pdfAjaxUrl,
			type : 'post',
			data : {
				action : 'give_pdf_order',
				order_list : listArray
			},
			success : function( response ) {
				hr("#muList").html("");
				var valTmp, htmlTmp = '';
				var t2 = response.replace('[','').replace(']','');
				var tpArray = t2.split(',');
				tpArray.forEach(function(element) {
					valTmp = element.replace('"','').replace('"','');
					htmlTmp = "<li id='my_list_item_"+valTmp+"'><img src='<?php echo get_template_directory_uri();?>/assets/images/arrow.png' alt='move' width='16' height='16' class='handle' />"+valTmp+"</li>";
					if(valTmp != ""){hr("#muList").append(htmlTmp);}
				}, this);
				alert('Orden gerenarado Exitosamente');
				window.location = redirectURL;
			}
		});

	});

	hr('.ifShowed').click(function(e){
		hr('.make-pdf').fadeIn();
	})
});
</script>
<?php get_footer();?>
