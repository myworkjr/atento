<?php
/**
 * Template Name: Upload Test
 *
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */ 
if ( !is_user_logged_in() ) { wp_redirect( home_url('/index.php/ingreso/') ); exit;}
global $current_user;
acf_form_head();
get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <section>
                
                <?php		
                    echo "<h2>Descarge el siguiente archivo y subalo contestado</h2>";
                    echo "<h2><a href='http://atento.creatucv.com/test/test.xlsx' target='_blank'>Descargar</a></h2>";

                if (getExamenLink($current_user->ID)>0):
                    $pdfDownloadURL="http://atento.creatucv.com/wp-content/uploads/cvs/"."ctcv-".$current_user->ID.".pdf";
                    echo "<h2>Ya Tiene Papeleria Subida</h2>";
                    echo "<h2>Su codigo de Descarga: <strong>ctcv-$current_user->ID</strong></h2>";
                    echo "<h2><a href='$pdfDownloadURL' target='_blank'>Descargar</a>  |  ".getPdfLink($current_user->ID)."</h2>";
                else:
                    acf_form(array(
                        'post_id'		=> 'new_post',
                        'post_title'	=> false,
                        'post_content'	=> false,
                        'field_groups'       => array(179),
                        'return'             => '%post_url%',
                        'html_submit_button'	=> '<input type="submit" class="acf-button button btn button-primary button-large" value="%s" />',
                        'html_submit_spinner'	=> '<span class="acf-spinner"></span>',
                        'submit_value' => __("Subir Respuestas", 'acf'),
                        'new_post'		=> array(
                            'post_type'		=> 'examenes',
                            'post_status'	=> 'publish'
                        )
                    ));
                endif;
                ?>
            </section>
		</main><!-- #main -->
	</div><!-- #primary -->
<style>
.acf-form-submit {
    display: block;
    text-align: right;
}
</style>    	
</div><!-- .wrap -->
<?php get_footer();