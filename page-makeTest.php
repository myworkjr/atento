<?php
/**
 * Template Name: Make Test
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */

if( $current_user->has_cap( 'administrator') or $current_user->has_cap( 'hr_admin')) {$isHable = true;}
else{ wp_redirect( home_url() ); exit;}
 global $current_user, $wp_roles;
 acf_form_head(); 
get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php			
                    echo "<h2>Descarge el siguiente archivo para crear examenes.</h2>";
                    echo "<h2><a href='http://atento.creatucv.com/test/customTestMookUp.csv' target='_blank'>Descargar</a></h2>";


				acf_form(array(
					'post_id'		=> 'new_post',
					'post_title'	=> true,
					'post_content'	=> false,
					'field_groups'       => array(192),
					'return'             => '%post_url%',
					'html_submit_button'	=> '<input type="submit" class="acf-button button btn button-primary button-large" value="%s" />',
					'html_submit_spinner'	=> '<span class="acf-spinner"></span>',
					'submit_value' => __("Create Test", 'acf'),
					'new_post'		=> array(
						'post_type'		=> 'create_tests',
						'post_status'	=> 'publish'
					)
				));
			?>
			<style>
			.acf-form-submit {
				display: block;
				text-align: right;
			}
			</style>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();?>