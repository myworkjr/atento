<?php
/**
 * Template Name: Get All Cvs
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */
 if ( !is_user_logged_in() ) { wp_redirect( home_url('/index.php/ingreso/') ); exit;}
get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <?php 
            $posts = get_posts(array(
                'posts_per_page'	=> -1,
                'post_type'			=> 'cv'
            ));
            if( $posts ): ?>
                <ul>
                <?php foreach( $posts as $post ): ?>
                    <li>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                <?php endforeach; ?>
                </ul>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();?>