<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
if ( !is_user_logged_in() ) { wp_redirect( home_url('/index.php/ingreso/') ); exit;}
acf_form_head();
get_header(); ?>
<?php
    $step1_url="#1";
    $step2_url="#2";
    $step3_url="#3";
    $step4_url="#4";
    $step5_url="#5";
	$cvOrbitPreFix = "http://ver.creatucv.com/orbit/index.php?cvid=".get_the_ID();
	$cvPillarPreFix = "http://ver.creatucv.com/pillar/index.php?cvid=".get_the_ID();
	$cvOnline = "http://ver.creatucv.com/start/index.php?cvid=".get_the_ID();
?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<section>
			<div class="optionContainer col-md-10">
				<?php for($i=1;$i<=6;$i++):?>
				<div class="cv_opts cvOption<?php echo $i;?>"><a href="<?php echo $cvOrbitPreFix."&type=$i";?>" target="_blank">Opcion <?php echo $i;?></a></div>
				<?php endfor; ?>
			</div>
			<div class="optionContainer col-md-10">
				<?php for($i=1;$i<=6;$i++):?>
				<div class="cv_opts cvPillarOption<?php echo $i;?>"><a href="<?php echo $cvPillarPreFix."&type=$i";?>" target="_blank">Opcion <?php echo $i+6;?></a></div>
				<?php endfor; ?>
			</div>			
		</section>
		<section>
			<div class="cvPort"><a href="<?php echo $cvOnline;?>" target="_blank">Online Portafolio</a></div>
		</section>
        <div class="stepwizard col-md-7">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="<?php echo $step1_url;?>" type="button" class="btn btn-primary btn-circle" id="1">1</a>
                </div>
                <div class="stepwizard-step">
                    <a href="<?php echo $step2_url;?>" type="button" class="btn btn-default btn-circle" disabled="disabled" id="2">2</a>
                </div>
                <div class="stepwizard-step">
                    <a href="<?php echo $step3_url;?>" type="button" class="btn btn-default btn-circle" disabled="disabled" id="3">3</a>
                </div>
                <div class="stepwizard-step">
                    <a href="<?php echo $step4_url;?>" type="button" class="btn btn-default btn-circle" disabled="disabled" id="4">4</a>
                </div>
                <div class="stepwizard-step">
                    <a href="<?php echo $step5_url;?>" type="button" class="btn btn-default btn-circle" disabled="disabled" id="5">5</a>
                </div>
            </div>
        </div>
			<?php			
				acf_form(array(
					'post_id'		=> get_the_ID(),
					'post_title'	=> false,
					'post_content'	=> false,
					'field_groups'       => array(10),
					'return'             => '%post_url%',
					'html_submit_button'	=> '<input type="submit" class="acf-button button btn button-primary button-large" value="%s" />',
					'html_submit_spinner'	=> '<span class="acf-spinner"></span>',
					'submit_value' => __("Actualizar CV", 'acf'),
					'new_post'		=> array(
						'post_type'		=> 'cv',
						'post_status'	=> 'publish'
					)
				));
			?>
			<div>
				<button type="button" class="btn btn-primary btn-lg navBt btPrevF">
				<span class="glyphicon glyphicon glyphicon-backward" aria-hidden="true"></span> Prev
				</button>
				<button type="button" class="btn btn-primary btn-lg navBt btNextF">
				<span class="glyphicon glyphicon glyphicon-forward" aria-hidden="true"></span> Next
				</button>    
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<script>
	jt = jQuery.noConflict();
	jt(document).ready(function(){
		jt(".step3, .step2, .step4, .step5").hide();
	});
</script>
<?php get_footer();