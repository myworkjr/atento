<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function twentyseventeen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'twentyseventeen' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'twentyseventeen' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'twentyseventeen-featured-image', 2000, 1200, true );

	add_image_size( 'twentyseventeen-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'twentyseventeen' ),
		'social' => __( 'Social Links Menu', 'twentyseventeen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', twentyseventeen_fonts_url() ) );

	// Define and register starter content to showcase the theme on new sites.
	$starter_content = array(
		'widgets' => array(
			// Place three core-defined widgets in the sidebar area.
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			// Add the core-defined business info widget to the footer 1 area.
			'sidebar-2' => array(
				'text_business_info',
			),

			// Put two core-defined widgets in the footer 2 area.
			'sidebar-3' => array(
				'text_about',
				'search',
			),
		),

		// Specify the core-defined pages to create and add custom thumbnails to some of them.
		'posts' => array(
			'home',
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		// Create the custom image attachments used as post thumbnails for pages.
		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/sandwich.jpg',
			),
			'image-coffee' => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/coffee.jpg',
			),
		),

		// Default to a static front page and assign the front and posts pages.
		'options' => array(
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		// Set the front page section theme mods to the IDs of the core-registered pages.
		'theme_mods' => array(
			'panel_1' => '{{homepage-section}}',
			'panel_2' => '{{about}}',
			'panel_3' => '{{blog}}',
			'panel_4' => '{{contact}}',
		),

		// Set up nav menus for each of the two areas registered in the theme.
		'nav_menus' => array(
			// Assign a menu to the "top" location.
			'top' => array(
				'name' => __( 'Top Menu', 'twentyseventeen' ),
				'items' => array(
					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
					'page_about',
					'page_blog',
					'page_contact',
				),
			),

			// Assign a menu to the "social" location.
			'social' => array(
				'name' => __( 'Social Links Menu', 'twentyseventeen' ),
				'items' => array(
					'link_yelp',
					'link_facebook',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	);

	/**
	 * Filters Twenty Seventeen array of starter content.
	 *
	 * @since Twenty Seventeen 1.1
	 *
	 * @param array $starter_content Array of starter content.
	 */
	$starter_content = apply_filters( 'twentyseventeen_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );
}
add_action( 'after_setup_theme', 'twentyseventeen_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function twentyseventeen_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( twentyseventeen_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Twenty Seventeen content width of the theme.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'twentyseventeen_content_width', $content_width );
}
add_action( 'template_redirect', 'twentyseventeen_content_width', 0 );

/**
 * Register custom fonts.
 */
function twentyseventeen_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Libre Franklin, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'twentyseventeen' );

	if ( 'off' !== $libre_franklin ) {
		$font_families = array();

		$font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function twentyseventeen_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'twentyseventeen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'twentyseventeen_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentyseventeen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'twentyseventeen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'twentyseventeen' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'twentyseventeen' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentyseventeen_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function twentyseventeen_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'twentyseventeen_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function twentyseventeen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyseventeen_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function twentyseventeen_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'twentyseventeen_pingback_header' );

/**
 * Display custom color CSS.
 */
function twentyseventeen_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );
?>
	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>
		<?php echo twentyseventeen_custom_colors_css(); ?>
	</style>
<?php }
add_action( 'wp_head', 'twentyseventeen_colors_css_wrap' );

/**
 * Enqueue scripts and styles.
 */
function twentyseventeen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentyseventeen-fonts', twentyseventeen_fonts_url(), array(), null );

	// Theme stylesheet.
	wp_enqueue_style( 'twentyseventeen-style', get_stylesheet_uri() );

	wp_enqueue_style( 'bootstrap', get_theme_file_uri( '/assets/css/bootstrap.css' ), array( 'twentyseventeen-style' ), '1.0' );
	// Load the dark colorscheme.
	if ( 'dark' === get_theme_mod( 'colorscheme', 'light' ) || is_customize_preview() ) {
		wp_enqueue_style( 'twentyseventeen-colors-dark', get_theme_file_uri( '/assets/css/colors-dark.css' ), array( 'twentyseventeen-style' ), '1.0' );
	}

	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
	if ( is_customize_preview() ) {
		wp_enqueue_style( 'twentyseventeen-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'twentyseventeen-style' ), '1.0' );
		wp_style_add_data( 'twentyseventeen-ie9', 'conditional', 'IE 9' );
	}

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentyseventeen-ie8', get_theme_file_uri( '/assets/css/ie8.css' ), array( 'twentyseventeen-style' ), '1.0' );
	wp_style_add_data( 'twentyseventeen-ie8', 'conditional', 'lt IE 9' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );


	wp_enqueue_style( 'font-awsome-cdn',  'https://use.fontawesome.com/e41e26731d.css');

	wp_enqueue_script( 'twentyseventeen-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );

	$twentyseventeen_l10n = array(
		'quote'          => twentyseventeen_get_svg( array( 'icon' => 'quote-right' ) ),
	);

		//if(!is_page('hr-module')):
			if ( has_nav_menu( 'top' ) ) {
				wp_enqueue_script( 'twentyseventeen-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );
				$twentyseventeen_l10n['expand']         = __( 'Expand child menu', 'twentyseventeen' );
				$twentyseventeen_l10n['collapse']       = __( 'Collapse child menu', 'twentyseventeen' );
				$twentyseventeen_l10n['icon']           = twentyseventeen_get_svg( array( 'icon' => 'angle-down', 'fallback' => true ) );
			}

			wp_enqueue_script( 'twentyseventeen-global', get_theme_file_uri( '/assets/js/global.js' ), array( 'jquery' ), '1.0', true );

			wp_enqueue_script( 'jquery-scrollto', get_theme_file_uri( '/assets/js/jquery.scrollTo.js' ), array( 'jquery' ), '2.1.2', true );

			wp_enqueue_script( 'jquery-steps', get_theme_file_uri( '/assets/js/main.js' ), array( 'jquery' ), '2.1.2', true );
		//else:
/*			wp_deregister_script( 'jquery' );
			wp_register_script( 'jquery-new', get_template_directory_uri() . '/assets/js/jquery-1.3.2.min.js' );		
			wp_register_script( 'jquery-iu', get_template_directory_uri() . '/assets/js/jquery-ui-1.7.1.custom.min.js',array('jquery-new') );*/	
			
		//endif;
		
	wp_localize_script( 'twentyseventeen-skip-link-focus-fix', 'twentyseventeenScreenReaderText', $twentyseventeen_l10n );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'twentyseventeen_scripts' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentyseventeen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentyseventeen_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function twentyseventeen_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'twentyseventeen_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function twentyseventeen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentyseventeen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function twentyseventeen_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'twentyseventeen_front_page_template' );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Seventeen 1.4
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function twentyseventeen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentyseventeen_widget_tag_cloud_args' );

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );


// Function to change email address
 
 function wpb_sender_email( $original_email_address ) {
     return 'info@creatucv.com';
 }
 
// Function to change sender name
function wpb_sender_name( $original_email_from ) {
    return 'Crea Tu CV';
}
 
// Hooking up our functions to WordPress filters 
 add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );




add_action('acf/save_post', 'custom_save_function');

function custom_save_function( $post_id ) {
	global $current_user;	

	if( get_post_type($post_id) == 'cv' ) {
		$candidato = get_post($post_id);
		$datos = get_field('informacion_personal',$post_id);
		$title = $datos['dpi_o_pasaporte']." | ".$datos['nombre']." ".$datos['apellido'];
		$my_post = array();
    	$my_post['ID'] = $post_id;
  	    $my_post['post_title'] = $title;
		wp_update_post( $my_post );
		return;
	}	

	if( get_post_type($post_id) == 'papeleria' ) {
		$title = $current_user->data->display_name." | ".date('Y-m-d H:i:s');
		$my_post = array();
    	$my_post['ID'] = $post_id;
  	    $my_post['post_title'] = $title;
		wp_update_post( $my_post );
		createPdfForCV($post_id);
		return;
	}


	if( get_post_type($post_id) == 'create_tests' ) {
		$title = "Test Created by: ".$current_user->data->display_name." | ".date('Y-m-d H:i:s');
		$my_post = array();
    	$my_post['ID'] = $post_id;
  	    $my_post['post_title'] = $title;
		//wp_update_post( $my_post );		
		return;
	}

	// bail early if not a contact_form post
	if( get_post_type($post_id) !== 'contact_form' ) {
		
		return;
		
	}
	
	
	// bail early if editing in admin
	if( is_admin() ) {
		
		return;
		
	}
	

	
	// vars
	// $post = get_post( $post_id );
	
	
	// // get custom fields (field group exists for content_form)
	// $name = get_field('name', $post_id);
	// $email = get_field('email', $post_id);
	
	
	// // email data
	// $to = 'contact@website.com';
	// $headers = 'From: ' . $name . ' <' . $email . '>' . "\r\n";
	// $subject = $post->post_title;
	// $body = $post->post_content;
	
	
	// // send email
	// wp_mail($to, $subject, $body, $headers );
	
}


function hide_admin_bar_from_front_end(){
  if (is_blog_admin()) {
    return true;
  }
  return false;
}
add_filter( 'show_admin_bar', 'hide_admin_bar_from_front_end' );


function createPdfForCV($theID){
		$fields = get_fields($theID);
		global $current_user;
		$tempPDFArray = array();
		$imgToPdfString = "convert ";
		$cvsUploadFolder = WP_CONTENT_DIR."/uploads/cvs/";
		$pdfToSave = $cvsUploadFolder."ctcv-".$current_user->ID.".pdf";
		$siteUrl = 'https://atento.creatucv.com/wp-content';
			foreach($fields as $k=>$v):
				if(isset($v)):
					switch ($k) {
						case 'dpi':
							if(($v['mime_type']=="image/jpeg")||($v['mime_type']=="application/pdf")||($v['mime_type']=="image/png")){
								$tempPDFArray['dpi'] = $v['url'];
								$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v['url'])." ";
							}
							break;
						case 'cv':
							if(($v['mime_type']=="image/jpeg")||($v['mime_type']=="application/pdf")||($v['mime_type']=="image/png")){
								$tempPDFArray['cv'] = $v['url'];
								$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v['url'])." ";
							}
							break;
						case 'antecedentes_penales':
							if(($v['boleta']['mime_type']=="image/jpeg")||($v['boleta']['mime_type']=="application/pdf")||($v['boleta']['mime_type']=="image/png")){
								$tempPDFArray['antecedentes_penales'] = $v['boleta']['url'];
								$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v['boleta']['url'])." ";
							}									
							break;
						case 'antecedentes_policiacos':
							if(($v['boleta']['mime_type']=="image/jpeg")||($v['boleta']['mime_type']=="application/pdf")||($v['boleta']['mime_type']=="image/png")){
								$tempPDFArray['antecedentes_policiacos'] = $v['boleta']['url'];
								$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v['boleta']['url'])." ";
							}	
							break;
						case 'cartas':
							if(isset($v['laborales'][0]['carta'])){
								foreach ($v['laborales'] as $m) {
									if(isset($m['carta']['url'])){
										if(($m['carta']['mime_type']=="image/jpeg")||($m['carta']['mime_type']=="application/pdf")||($m['carta']['mime_type']=="image/png")){
											$tempPDFArray['laborales'][] = $m['carta']['url'];
											$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$m['carta']['url'])." ";
										}
									}
								}
							}
							if(isset($v['recomendaciones_personales'][0]['carta'])){
								foreach ($v['recomendaciones_personales'] as $m) {
									if(isset($m['carta']['url'])){
										if(($m['carta']['mime_type']=="image/jpeg")||($m['carta']['mime_type']=="application/pdf")||($m['carta']['mime_type']=="image/png")){
											$tempPDFArray['recomendaciones_personales'][] = $m['carta']['url'];
											$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$m['carta']['url'])." ";
										}
									}
								}
							}									
							break;	
						case 'diplomas':
							foreach($v as $n){
								if(isset($n['diploma']['url'])){
									if(($n['diploma']['mime_type']=="image/jpeg")||($n['diploma']['mime_type']=="application/pdf")||($n['diploma']['mime_type']=="image/png")){
										$tempPDFArray['diplomas'][] = $n['diploma']['url'];
										$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$n['diploma']['url'])." ";
									}												
								}
							}
							break;																																												
						case 'nit':
							if(($v['mime_type']=="image/jpeg")||($v['mime_type']=="application/pdf")||($v['mime_type']=="image/png")){
								$tempPDFArray['nit'] = $v['url'];
								$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v['url'])." ";
							}								
							break;	
						case 'igss':
							if(($v['mime_type']=="image/jpeg")||($v['mime_type']=="application/pdf")||($v['mime_type']=="image/png")){
								$tempPDFArray['igss'] = $v['url'];
								$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v['url'])." ";
							}									
							break;	
						case 'record_crediticio':
							if(($v['mime_type']=="image/jpeg")||($v['mime_type']=="application/pdf")||($v['mime_type']=="image/png")){
								$tempPDFArray['record_crediticio'] = $v['url'];
								$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v['url'])." ";
							}									
							break;	
						case 'recibos':
							if(isset($v['agua']['url'])){
								if(($v['agua']['mime_type']=="image/jpeg")||($v['agua']['mime_type']=="application/pdf")||($v['agua']['mime_type']=="image/png")){
									$tempPDFArray['recibos'][] = $v['agua']['url'];
									$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v['agua']['url'])." ";
								}
							}
							if(isset($v['luz']['url'])){
								if(($v['luz']['mime_type']=="image/jpeg")||($v['luz']['mime_type']=="application/pdf")||($v['luz']['mime_type']=="image/png")){
									$tempPDFArray['recibos'][] = $v['luz']['url'];
									$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v['luz']['url'])." ";
								}
							}
							if(isset($v['telefono']['url'])){
								if(($v['telefono']['mime_type']=="image/jpeg")||($v['telefono']['mime_type']=="application/pdf")||($v['telefono']['mime_type']=="image/png")){
									$tempPDFArray['recibos'][] = $v['telefono']['url'];
									$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v['telefono']['url'])." ";
								}
							}
							break;										
						default:
							# code...
							break;
					}
				endif;
			endforeach;
			update_user_meta($current_user->ID,'pdf_images',json_encode($tempPDFArray));
			//update_user_meta($current_user->ID,'pdf_images',$tempPDFArray);
			$imgToPdfString .= " ".$pdfToSave;
		//	echo $imgToPdfString;
			exec($imgToPdfString);
		//	die();
}

function cuantasPapelerias($id){
	$args = array(
		'numberposts'	=> -1,
		'post_type'		=> 'papeleria',
		 'author'        => $id
	);
	$evaluaciones = new WP_Query($args); 
	return $evaluaciones->post_count;
}


function cuantosExamen($id){
	$args = array(
		'numberposts'	=> -1,
		'post_type'		=> 'examenes',
		 'author'        => $id
	);
	$evaluaciones = new WP_Query($args); 
	return $evaluaciones->post_count;
}


function cuantosCVs($id){
	$args = array(
		'numberposts'	=> -1,
		'post_type'		=> 'cv',
		 'author'        => $id
	);
	$evaluaciones = new WP_Query($args); 
	return $evaluaciones->post_count;
}

function getPdfLink($id){
		$posts = get_posts(array(
			'numberposts'	=> 1,
			'post_type'		=> 'papeleria',
			'author'        => $id
		));
		if( $posts ): 
			foreach( $posts as $post ): 
				$theLink = " <a href='$post->guid'>Editar Papeleria</a>";      
			endforeach;
			wp_reset_postdata();
		endif;
		return $theLink;
}


function getExamenLink($id){
		$posts = get_posts(array(
			'numberposts'	=> 1,
			'post_type'		=> 'examenes',
			'author'        => $id
		));
		if( $posts ): 
			foreach( $posts as $post ): 
				$value = get_field( "examen", $post->ID );
		
				$theLink = " <a href='$value'>Descargar Examen</a>";  			
			endforeach;
			wp_reset_postdata();
		endif;
		return $theLink;
}


//******Roles For Users */
add_role(
    'hr_admin',
    __( 'HR Team Admin' ),
    array(
        'read'         			=> true,  
        'edit_posts'   			=> true,
		'edit_published_posts'	=> true,
		'upload_files'			=> true,
		'publish_posts'			=> true,
		'read_private_pages'	=> true,
		'list_users'			=> true,
		'edit_others_posts'		=> true
    )
);

add_role(
    'wave_coach',
    __( 'Wave Coach' ),
    array(
        'read'         			=> true,  // true allows this capability
        'edit_posts'   			=> true,
		'edit_published_posts'	=> true,
		'upload_files'			=> true,
		'publish_posts'			=> true,
		'read_private_pages'	=> true,
		'list_users'			=> true,
		'edit_others_posts'		=> true
    )
);


add_role(
    'cuenta_manager	',
    __( 'Manager de Cuenta' ),
    array(
        'read'         			=> true,  // true allows this capability
        'edit_posts'   			=> true,
		'edit_published_posts'	=> true,
		'upload_files'			=> true,
		'publish_posts'			=> true,
		'read_private_pages'	=> true,
		'list_users'			=> true,
		'edit_others_posts'		=> true
    )
);

//add_action( 'wp_ajax_give_pdf_order', 'give_pdf_order' );
function custom_pdf_order_hr($order, $userID){
	global $current_user;
	$downlodCode = "ctcv-".$userID;
	$pdfDownloadURL="http://atento creatucv.com/wp-content/uploads/cvs/"."ctcv-".$userID.".pdf";

	$filesArray = array('dpi','cv','antecedentes_penales','antecedentes_policiacos','cartas','diplomas','nit','igss','record_crediticio','recibos');
	$myOrder = get_user_meta($order,'pdf_order');	
	$userData = (array)json_decode(get_user_meta($userID,'pdf_images',true));	
	$cvsUploadFolder = WP_CONTENT_DIR."/uploads/cvs/";
	$pdfToSave = $cvsUploadFolder."Custom-ctcv-".$userID."-".$order.".pdf";
	$imgToPdfString = "convert ";
	$myOrderArray = explode(',',$myOrder[0]);
	$siteUrl = 'https://atento.creatucv.com/wp-content';
	foreach ($myOrderArray as $key => $value) {
		if(($value == "diplomas")||($value == "recibos")){
			foreach($userData[$value] as $k => $v){
					$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v)." ";
			}
		}elseif($value == "cartas"){
			foreach($userData["laborales"] as $k => $v){
				$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v)." ";
			}			
			foreach($userData["recomendaciones_personales"] as $k => $v){
				$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$v)." ";
			}				
		}else{
			$imgToPdfString .= str_replace($siteUrl,WP_CONTENT_DIR,$userData[$value])." ";
		}
	}
	$imgToPdfString .= " ".$pdfToSave;
	exec($imgToPdfString);
}


//add_action( 'wp_ajax_nopriv_give_pdf_order', 'give_pdf_order' );
add_action( 'wp_ajax_give_pdf_order', 'give_pdf_order' );

function give_pdf_order() {
	global $current_user;
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { 
		$temp = explode(',',$_REQUEST['order_list']);
		if( $current_user->has_cap( 'administrator') or $current_user->has_cap( 'hr_admin')) {
			$userData = get_user_meta($current_user->ID,'pdf_order');
			update_user_meta($current_user->ID,'pdf_order',$_REQUEST['order_list']);
		}
		echo json_encode($temp);		
	}
die();	
}

function callFromPage($uid){
	global $current_user;
	$userData = get_user_meta($current_user->ID,'pdf_images');		
		if(empty($userData)){
			$posts = get_posts(array(
				'numberposts'	=> 1,
				'post_type'		=> 'papeleria',
				'author'        => $current_user->ID
			));
			foreach( $posts as $post ):
				createPdfForCV($post->ID);
			endforeach;
		}
}


function getCustomTest(){
	//$textTemp = "<ul>";
	$textTemp = "";
		$posts = get_posts(array(
			'numberposts'	=> -1,
			'post_type'		=> 'create_tests'
		));
		foreach( $posts as $post ):
			if($post->post_title != ""){
				$textTemp .= "<li><a href='".$post->guid."'>".$post->post_title."</a></li>";
			}
		endforeach;
		//$textTemp .= "</ul>";
	return $textTemp;
}


//add_action( 'wp_ajax_nopriv_give_pdf_order', 'give_pdf_order' );
add_action( 'wp_ajax_cv_pdf_download', 'cv_pdf_download' );

function cv_pdf_download() {
	global $current_user;
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { 
		$filesToZip = explode(',',$_REQUEST['order_list']);
		if( $current_user->has_cap( 'administrator') or $current_user->has_cap( 'hr_admin')) {
				$directory = WP_CONTENT_DIR."/uploads/";
				$command = "cd ".WP_CONTENT_DIR."; ";
				$strTemp = "";
				$zipname = $_REQUEST['zipname']?"uploads/cvs_pdf/".$_REQUEST['zipname']."zip":"uploads/cvs_pdf/".$current_user->data->user_login.time().".zip";
				$command .= "zip -9 -r ".$zipname;
				foreach ($filesToZip as $file) {
					custom_pdf_order_hr($current_user->ID, $file);
				}
				foreach ($filesToZip as $file) {
					$strTemp .= " ".$directory."cvs/ctcv-".$file.".pdf ";
				}
				$command .= " ".$strTemp;
				//echo $command;
				exec($command);
		}
		echo "https://atento.creatucv.com/wp-content/$zipname";
	}
die();	
}



//add_action( 'wp_ajax_nopriv_give_pdf_order', 'give_pdf_order' );
add_action( 'wp_ajax_hr_cv_pdf_download', 'hr_cv_pdf_download' );

function hr_cv_pdf_download() {
	global $current_user;
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { 
		$filesToZip = explode(',',$_REQUEST['order_list']);
		$filesToPDF = explode(',',$_REQUEST['order_list']);
		$fileName = $_REQUEST['zipname']?$_REQUEST['zipname']:$current_user->data->user_login;
		$fileName .= time();
		if( $current_user->has_cap( 'administrator') or $current_user->has_cap( 'hr_admin')) {
				$directory = WP_CONTENT_DIR."/uploads/";
				$command = "cd ".WP_CONTENT_DIR."; ";
				$strTemp = "";
				//$zipname = "uploads/cvs_pdf/".$current_user->data->user_login.time().".zip";
				$zipname = "uploads/cvs_pdf/".$fileName."v.zip";
				$command .= "zip -9 -r ".$zipname;

				foreach ($filesToPDF as $file) {
					custom_pdf_order_hr($current_user->ID, $file);
				}
				$waveName ='Custom';
				foreach ($filesToZip as $file) {
					$strTemp .= " ".$directory."cvs/$waveName-ctcv-".$file."-".$current_user->ID.".pdf ";
				}
				$command .= " ".$strTemp;

				exec($command);
//				echo ($command);
//				die();

		}
		echo "https://atento.creatucv.com/wp-content/$zipname";
	}
die();	
}


add_action( 'wp_ajax_creatucv_add_new_hr_member', 'creatucv_add_new_hr_member' );
function creatucv_add_new_hr_member() {
  	if (isset( $_POST["creatucv_user_login"] ) && wp_verify_nonce($_POST['creatucv_register_nonce'], 'creatucv-register-nonce')) {
		$user_login		= $_REQUEST["creatucv_user_login"];	
		$user_email		= $_REQUEST["creatucv_user_email"];
		$user_first 	= $_REQUEST["creatucv_user_first"];
		$user_last	 	= $_REQUEST["creatucv_user_last"];
		$user_pass		= $_REQUEST["creatucv_user_pass"];
		$pass_confirm 	= $_REQUEST["creatucv_user_pass_confirm"];
 
		// this is required for username checks
		require_once(ABSPATH . WPINC . '/registration.php');
 
		if(username_exists($user_login)) {
			// Username already registered
			//creatucv_errors()->add('username_unavailable', __('Username already taken'));
		}
		if(!validate_username($user_login)) {
			// invalid username
			//creatucv_errors()->add('username_invalid', __('Invalid username'));
		}
		if($user_login == '') {
			// empty username
			//creatucv_errors()->add('username_empty', __('Please enter a username'));
		}
		if(!is_email($user_email)) {
			//invalid email
			//creatucv_errors()->add('email_invalid', __('Invalid email'));
		}
		if(email_exists($user_email)) {
			//Email address already registered
			//creatucv_errors()->add('email_used', __('Email already registered'));
		}
		if($user_pass == '') {
			// passwords do not match
			//creatucv_errors()->add('password_empty', __('Please enter a password'));
		}
		if($user_pass != $pass_confirm) {
			// passwords do not match
			//creatucv_errors()->add('password_mismatch', __('Passwords do not match'));
		}
 
		//$errors = creatucv_errors()->get_error_messages();
 
		// only create the user in if there are no errors
		if(empty($errors)) {
 
			$new_user_id = wp_insert_user(array(
					'user_login'		=> $user_login,
					'user_pass'	 		=> $user_pass,
					'user_email'		=> $user_email,
					'first_name'		=> $user_first,
					'last_name'			=> $user_last,
					'user_registered'	=> date('Y-m-d H:i:s'),
					'role'				=> 'hr_admin'
				)
			);
			if($new_user_id) {
				// send an email to the admin alerting them of the registration
				wp_new_user_notification($new_user_id);
				echo "Usuario Creado";

			}
 
		}else{echo "error";}
 
	}
	die();	
}

function gotPapeleria($user){
	$return = "Papeleria: ";
	$userData = (array)json_decode(get_user_meta($user,'pdf_images',true));	
	$por = round((count($userData)*100)/11);
	$cartas = (isset($userData['laborales']))?count($userData['laborales']):0;
	$personales = (isset($userData['recomendaciones_personales']))?count($userData['recomendaciones_personales']):0;
	$diplomas = (isset($userData['diplomas']))?count($userData['diplomas']):0;
	$recibos = (isset($userData['recibos']))?count($userData['recibos']):0;
	$return .= "$por % | Cartas Laborales: $cartas | Cartas Personales: $personales | Diplomas: $diplomas | Recibos: $recibos";
	return $return;
}

add_action( 'wp_ajax_creatucv_typing_test', 'creatucv_typing_test' );
function creatucv_typing_test() {
	global $current_user;
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { 
		$result['wpm'] = $_REQUEST['wpm']?$_REQUEST['wpm']:0;
		$result['wc'] = $_REQUEST['wc']?$_REQUEST['wc']:0;
		$result['we'] = $_REQUEST['we']?$_REQUEST['we']:0;
		//$userData = get_user_meta($current_user->ID,'typing_test');
		update_user_meta($current_user->ID,'typing_test',$result);
		echo 'true';		
	}else{echo 'false';}
die();
}


function readCSV($csvFile){
	$file_handle = @fopen($csvFile, 'rb');
	while (!feof($file_handle) ) {
		$line_of_text[] = fgetcsv($file_handle, 1024);
	}
	fclose($file_handle);
	return $line_of_text;
}

// Limit media library access
  
add_filter( 'ajax_query_attachments_args', 'wpb_show_current_user_attachments' );
 
function wpb_show_current_user_attachments( $query ) {
    $user_id = get_current_user_id();
    if ( $user_id && !current_user_can('activate_plugins') && !current_user_can('edit_others_posts
') ) {
        $query['author'] = $user_id;
    }
    return $query;
} 