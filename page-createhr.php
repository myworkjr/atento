<?php
/**
 * Template Name: Page HR Users
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */
/*if ( is_user_logged_in() ) { wp_redirect( home_url('/index.php/crear-mi-cv/') ); exit;}*/
get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<h3 class="creatucv_header"><?php _e('Register New Account'); ?></h3>
 		<form id="creatucv_hrusers_form" class="creatucv_form" action="" method="POST">
			<fieldset>
				<p>
					<label for="creatucv_user_Login"><?php _e('Username'); ?></label>
					<input name="creatucv_user_login" id="creatucv_user_login" class="required" type="text"/>
				</p>
				<p>
					<label for="creatucv_user_email"><?php _e('Email'); ?></label>
					<input name="creatucv_user_email" id="creatucv_user_email" class="required" type="email"/>
				</p>
				<p>
					<label for="creatucv_user_first"><?php _e('First Name'); ?></label>
					<input name="creatucv_user_first" id="creatucv_user_first" type="text"/>
				</p>
				<p>
					<label for="creatucv_user_last"><?php _e('Last Name'); ?></label>
					<input name="creatucv_user_last" id="creatucv_user_last" type="text"/>
				</p>
				<p>
					<label for="password"><?php _e('Password'); ?></label>
					<input name="creatucv_user_pass" id="password" class="required" type="password"/>
				</p>
				<p>
					<label for="password_again"><?php _e('Password Again'); ?></label>
					<input name="creatucv_user_pass_confirm" id="password_again" class="required" type="password"/>
				</p>
				<p>
					<input type="hidden" name="action" value="creatucv_add_new_hr_member"/>
					<input type="hidden" name="creatucv_register_nonce" value="<?php echo wp_create_nonce('creatucv-register-nonce'); ?>"/>
					<input type="submit" value="<?php _e('Register Your Account'); ?>"/>
				</p>
			</fieldset>
		</form>
		</main><!-- #main -->
	</div><!-- #primary -->


        <script>
		var pdfAjaxUrl = "/wp-admin/admin-ajax.php";
            function getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
            }        
            jt = jQuery.noConflict();
            jt(document).ready(function(){
                jt("#creatucv_hrusers_form").submit(function(e){
                    e.preventDefault();
					console.log(jt(this).serializeArray());
					var theData = jt(this).serializeArray();
					jt.ajax({
						url : pdfAjaxUrl,
						type : 'post',
						data : theData,
						success : function( response ) {
							console.log(response);
						}
					});

                })
            });            
        </script>

</div><!-- .wrap -->
<?php get_footer();?>
