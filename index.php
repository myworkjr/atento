<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
 global $current_user;
if ( !is_user_logged_in() ) { wp_redirect( home_url('/index.php/ingreso/') ); exit;}
get_header(); ?>
<div class="wrap">
	<?php if ( is_home() && ! is_front_page() ) : ?>
		<header class="page-header">
			<h1 class="page-title"><?php single_post_title(); ?></h1>
		</header> 
	<?php else : ?>
	<header class="page-header">
		<h2 class="page-title"><?php _e( 'Posts', 'twentyseventeen' ); ?></h2>
	</header>
	<?php endif; ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


<?php	callFromPage(5);?>

	
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();?>