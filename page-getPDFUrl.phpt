<?php
/**
 * Template Name: Page Get PDF by Code
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */
 //if ( !is_user_logged_in() ) { wp_redirect( home_url('/ingreso/') ); exit;
 //if (is_user_logged_in()){wp_redirect(home_url();exit;}
 //if ( is_user_logged_in() ) { wp_redirect( home_url('/index.php/crear-mi-cv/') ); exit;}
get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <?php if ( !is_user_logged_in() ): ?>
        <h4 class="registration_link"><a href="<?php echo  home_url('/index.php/registrate/');?>"><?php _e('Nuevo Usuario?, Crea tu cuenta.'); ?></a></h4>
        <h4 class="registration_link"><a href="<?php echo  home_url('/index.php/ingreso/');?>"><?php _e('Ingresar'); ?></a></h4>
        <h3 class="creatucv_header"><?php _e('Descargar Papeleria'); ?></h3>
        <?php endif; ?>
		<form name="loginform" id="pdfform" action="" method="post">
			<p class="login-username">
				<label for="pdf_code">Ingrese el Codigo de Descarga</label>
				<input type="text" name="log" id="pdf_code" class="input" value="" size="20" />
			</p>
			<p class="login-submit">
				<input type="submit" name="wp-submit" id="wp-download-pdf" class="button button-primary" value="Descargar" />
			</p>
		</form>  
        <script>
            function getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
            }        
            jt = jQuery.noConflict();
            jt(document).ready(function(){
                jt("#pdfform").submit(function(e){
                    var code = jt('#pdf_code').val();
                    var pdfDwURL="http://creatucv.com/wp-content/uploads/cvs/"+code+".pdf";
                    e.preventDefault();
                    var scode = Math.round(getRandomArbitrary(10,50));
			var spacer = "";
                    var filePath = window.prompt("Ingrese el numero para poder descargar: "+scode,spacer);
                   // alert (filePath);
                    if(code != ""){
                        if(scode == filePath){
                            jt('<form target="_blank"></form>').attr('action', pdfDwURL).appendTo('body').submit().remove();          
                        }
                    }else{
                        alert('Ingrese un Codigo de descarga valido');                        
                    }
                })
            });            
        </script>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();?>
