<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
global $current_user;
?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="wrap">
				<?php
				get_template_part( 'template-parts/footer/footer', 'widgets' );

				if ( has_nav_menu( 'social' ) ) : ?>
					<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentyseventeen' ); ?>">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'social',
								'menu_class'     => 'social-links-menu',
								'depth'          => 1,
								'link_before'    => '<span class="screen-reader-text">',
								'link_after'     => '</span>' . twentyseventeen_get_svg( array( 'icon' => 'chain' ) ),
							) );
						?>
					</nav><!-- .social-navigation -->
				<?php endif;

				get_template_part( 'template-parts/footer/site', 'info' );
				?>
			</div><!-- .wrap -->
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->

<?php $typingData = get_user_meta($current_user->ID,'typing_test');?>
<?php if ( is_user_logged_in() ): ?>
<div class="page-wrapper chiller-theme">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
            <i class="fas fa-bars">+</i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand">
                    <a href="#">Crea tu CV</a>
                    <div id="close-sidebar">
                        <i class="fas fa-times">X</i>
                    </div>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/bootstrap4/assets/img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name"><?php echo $current_user->data->display_name;?></span>
                        <span class="user-role">Codigo de Descarga</span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span><?php echo "<strong>ctcv-$current_user->ID</strong>";?></span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-header  -->

                <!-- sidebar-search  -->
                <div class="sidebar-menu">
            <?php if($current_user->has_cap( 'hr_admin')):?>
            <?php
						$pdfDownloadURL = home_url('/index.php/hr-module/');
						$addNewCv = home_url('/index.php/descargar-cvs/');
						$addNewTest = home_url('/index.php/create-custom-test/');
						echo "<h2><a href='$pdfDownloadURL'>Orden de Papeleria</a>   |   <a href='$addNewCv' target='_blank'>Descargar Papeleria</a>   |   <a href='$addNewTest' target='_blank'>Crear Examen</a></h2>";	
            ?>
            <?php else: ?>
                    <ul>
                        <li class="header-menu">
                            <span>CV's</span>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fa fa-tachometer-alt"></i>
                                <span>Crear CV</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <?php
                                        if (cuantosCVs($current_user->ID)>0):
                                                $pdfDownloadURL = home_url('/index.php/ver-mis-cvs/');
                                                $addNewCv = home_url('/index.php/crear-mi-cv/');
                                                echo "<li><a href='$pdfDownloadURL'>Ver Mis CV's</a></li><li><a href='$addNewCv' target='_blank'>Crear CV</a></li>";			
                                        else:
                                                $pdfDownloadURL = home_url('/index.php/crear-mi-cv/');
                                                echo "<li><a href='$pdfDownloadURL' target='_blank'>Crear mi Primer CV</a> </li>";			
                                        endif;
                                    ?>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fa fa-shopping-cart"></i>
                                <span>Papeleria</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                <?php
                                    if (cuantasPapelerias($current_user->ID)>0):
                                            $pdfDownloadURL="http://atento.creatucv.com/wp-content/uploads/cvs/"."ctcv-".$current_user->ID.".pdf";
                                            echo "<li>Su codigo de Descarga: <strong>ctcv-$current_user->ID</strong></li>";
                                            echo "<li><a href='$pdfDownloadURL' target='_blank'>Descargar</a></li><li>".getPdfLink($current_user->ID)."</li>";
                                    else:
                                            $pdfDownloadURL = home_url('/index.php/mi-papeleria/');
                                            echo "<li><a href='$pdfDownloadURL' target='_blank'>Agregar Papeleria</a></li>";			
                                    endif;
                                ?>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="far fa-gem"></i>
                                <span>Examenes</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                <?php
                                        $pdfDownloadURL = home_url('/index.php/upload-test/');
                                        echo "<li><a href='$pdfDownloadURL' >Excel Test</a> </li>";	
                                        echo getCustomTest();
                                ?>                                    
                                </ul>
                            </div>
                        </li>
                        <li class="header-menu">
                            <span>Typing</span>
                        </li>
                        <li>
                        <?php
                            $pdfDownloadURL = home_url('/index.php/typing-test/');
                            echo "<a href='$pdfDownloadURL' ><span>Typing Test</span></a>";
                        ?>
                        </li>  
                        <?php  if(!empty($typingData)): ?>                      
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fa fa-chart-line"></i>
                                <span>Typing Results</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                            <?php
                                    echo "<li><strong>Words per Minute: </strong>".$typingData[0]['wpm']."</li><li><strong> Word Count: </strong>".$typingData[0]['wc']." </li><li><strong>Errors: </strong>".$typingData[0]['we']."</li>";
                            ?>
                                </ul>
                            </div>
                        </li>
                        <?php endif;?>
                    </ul>
            <?php endif;   ?>        
                </div>
                <!-- sidebar-menu  -->
            </div>
            <!-- sidebar-content  -->
            <div class="sidebar-footer">
                <a href="<?php echo wp_logout_url( home_url() ); ?>">
                    <i class="fa fa-bell"></i>
                    <span class="badge badge-pill badge-danger notification">Salir</span>
                </a>
                <a href="#">
                    <i class="fa fa-envelope"></i>
                    <span class="badge badge-pill badge-success notification">7</span>
                </a>
            </div>
        </nav>
        <!-- sidebar-wrapper  -->

        <!-- page-content" -->
    </div>
    <!-- page-wrapper -->
    <?php endif; ?>

		</main><!-- #main -->
<?php if ( is_user_logged_in() ): ?>
	<script>
    jq = jQuery.noConflict();
    jq(document).ready(function(){
        jq(".sidebar-dropdown > a").click(function() {
            jq(".sidebar-submenu").slideUp(200);
            if (
                jq(this)
                .parent()
                .hasClass("active")
            ) {
                jq(".sidebar-dropdown").removeClass("active");
                jq(this)
                .parent()
                .removeClass("active");
            } else {
                jq(".sidebar-dropdown").removeClass("active");
                jq(this)
                .next(".sidebar-submenu")
                .slideDown(200);
                jq(this)
                .parent()
                .addClass("active");
            }
        });

        jq("#close-sidebar").click(function() {
            jq(".page-wrapper").removeClass("toggled");
        });
        jq("#show-sidebar").click(function() {
            jq(".page-wrapper").addClass("toggled");
        });
		jq

});
    </script>
<?php endif; ?>	
<?php wp_footer(); ?>

</body>
</html>
