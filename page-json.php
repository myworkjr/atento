<?php
/**
 * Template Name: Json
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */
$theID = $_GET['cvid']?$_GET['cvid']:"0" ;
get_header("json"); ?>
<?php $fields = get_fields($theID); echo json_encode($fields);?>