jQuery.noConflict();
var currentStep = 1;

    jQuery(document).ready(function($) {
        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

        allWells.hide();
        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);
            pickStep($item.attr('id'));
            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });
        
        allPrevBtn.click(function(){
            

        });

        allNextBtn.click(function(){
            

        });

        //$('div.setup-panel div a.btn-primary').trigger('click');

        $(".btPrevF").click(function(e){
            e.preventDefault();
            console.log(currentStep);
            doClick(parseInt(currentStep)-1);
        });
        $(".btNextF").click(function(e){
            e.preventDefault();
            console.log(currentStep);
            doClick(parseInt(currentStep)+1);
        });        

        function pickStep(n){
            $(".step"+n).show();
            if(n==5){$('.acf-form-submit').show(); $('.btNextF').hide();}else{$('.acf-form-submit').hide(); $('.btNextF').show();}
            if(n==1){$('.btPrevF').hide();}else{$('.btPrevF').show();}            
            for( var i=1;i<=5;i++){
                if(n != i){
                    $(".step"+i).hide();
                }
            }
            currentStep = n;
        }

       function doClick(n){
           $("#"+n).click();
       }

    });