<?php
/**
 * Template Name: Page Registro
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */
if (is_user_logged_in()){wp_redirect(home_url());exit;}
get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <h4 class="registration_link"><a href="<?php echo  home_url('/index.php/ingreso/');?>"><?php _e('Ya eres Miembro?, Ingresa.'); ?></a></h4>
            <?php echo do_shortcode('[register_form]');?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();?>