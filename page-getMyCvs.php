<?php
/**
 * Template Name: Get My CVs
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */
 if ( !is_user_logged_in() ) { wp_redirect( home_url('/index.php/ingreso/') ); exit;}
 global $current_user, $wp_roles;
 if (cuantosCVs($current_user->ID)<=0){ wp_redirect( home_url('/index.php/crear-mi-cv/') ); exit;}
get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <?php 
            $posts = get_posts(array(
                'posts_per_page'	=> -1,
                'post_type'			=> 'cv',
                'author'        => $current_user->ID
            ));
            if( $posts ): ?>
                <ul>
                <?php foreach( $posts as $post ): ?>
                    <li>
                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    </li>
                <?php endforeach; ?>
                </ul>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
            <?php 
/*	global $current_user;
	$userData = get_user_meta($current_user->ID,'pdf_images');
    $urlLoop = json_decode($userData[0]);
    $filesToZip = array();
    echo "<pre>";
    foreach ($urlLoop as $key => $value) {
        print_r($key);
        if(!is_array($value)){
            $tp = str_replace('http://creatucv.com/wp-content',WP_CONTENT_DIR,$value)." ";
            //$tp = str_replace('http://creatucv.com/wp-content',"",$value)." ";
            echo $tp."<br>";
            array_push($filesToZip,$tp);
        }
    }
    
    //print_r(json_decode($userData[0]));
  */  echo "</pre>";
            ?>

            <?php
/*
$args = array(
    'role'    => 'author',
    'orderby' => 'user_nicename',
    'order'   => 'ASC'
);
$users = get_users( $args );

echo '<ul>';
foreach ( $users as $user ) {
    echo '<li>' . esc_html( $user->display_name ) . '[' . esc_html( $user->user_email ) . ']</li>';
}
echo '</ul>';
*/


/*class createDirZip extends createZip {
 
 function get_files_from_folder($directory, $put_into) {
 if ($handle = opendir($directory)) {
    while (false !== ($file = readdir($handle))) {
            if (is_file($directory.$file)) {
            $fileContents = file_get_contents($directory.$file);
            $this->addFile($fileContents, $put_into.$file);
            } elseif ($file != '.' and $file != '..' and is_dir($directory.$file)) {
                $this->addDirectory($put_into.$file.'/');
                $this->get_files_from_folder($directory.$file.'/', $put_into.$file.'/');
            }
    }
 }
 closedir($handle);
 }
}
*/

/*$zipname = WP_CONTENT_DIR.'/uploads/cvs_pdf/file.zip';
$zip = new ZipArchive;
//var_dump($zip);
echo "(";
$zip->open($zipname, ZipArchive::CREATE);
foreach ($filesToZip as $file) {
    echo "'$file',";
  $zip->addFile($file);
}
echo ")";
$zip->close();
var_dump(filesize($zipname));
header('Content-Type: application/zip');
header('Content-disposition: attachment; filename='.$zipname);
header('Content-Length: ' . filesize($zipname));
readfile($zipname);*/
/*$zip = new ZipArchive;
if ($zip->open('test_new.zip', ZipArchive::CREATE) === TRUE)
{
    // Add files to the zip file
    $zip->addFile('test.txt');
    $zip->addFile('test.pdf');
 
    // Add random.txt file to zip and rename it to newfile.txt
    $zip->addFile('random.txt', 'newfile.txt');
 
    // Add a file new.txt file to zip using the text specified
    $zip->addFromString('new.txt', 'text to be added to the new.txt file');
 
    // All files are added, so close the zip file.
    $zip->close();
}*/
/*
$directory = WP_CONTENT_DIR."/uploads/";
if ($handle = opendir($directory)) {
    $strTemp = "";
    $zipname = $directory."cvs_pdf/test4.zip";
    $command = "zip -9 -r ".$zipname;

    foreach ($filesToZip as $file) {
        $strTemp .= $file." ";

    }
    $command .= " ".$strTemp;
    echo $command;
    exec($command);
}
closedir($handle);*/
?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();?>