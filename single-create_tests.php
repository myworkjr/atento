<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
if ( !is_user_logged_in() ) { wp_redirect( home_url('/index.php/ingreso/') ); exit;}
acf_form_head();
get_header(); ?>
<?php
	global $current_user;
	$user = $current_user->ID;
	$fields = get_fields();
?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<section>
		</section>

		<?php
		$csv = readCSV($fields['file']);
		echo '<pre>';
		foreach ($csv as $key => $value) {
			$tmpArray = array();
			$title = "";
			$tmpText = "";
			$qn = $key;
			if($key > 0){
				foreach ($value as $k => $v){
					if($k == 0){
						$title = str_replace("<#%#>","______",$v);
					}else{
						$tmpArray[]=$v;
					}
				}
				shuffle($tmpArray);
				foreach ($tmpArray as $key => $value) {
					$tmpText .= "<div><input type='radio' id='question_$qn' name='question_$qn' value='$value'><label for='$value'>$value</label></div>";	
				}				
			}
			echo $title."<br>".$tmpText;
			echo "<br><br>";
		}
		echo '</pre>';
?>


		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();