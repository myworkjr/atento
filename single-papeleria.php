<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
if ( !is_user_logged_in() ) { wp_redirect( home_url('/index.php/ingreso/') ); exit;}
acf_form_head();
get_header(); ?>
<?php
/*	global $current_user;
	$user = $current_user->ID;
	$userData = (array)json_decode(get_user_meta($user,'pdf_images',true));	
echo "<pre>";	
	var_dump($userData);
	echo "<br>";
	echo count($userData);
echo "</pre>";*/
?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<section>
			<?php
                    echo "<h2>Su codigo de Descarga: <strong>ctcv-$current_user->ID</strong></h2>";
                    echo "<h2><a href='$pdfDownloadURL' target='_blank'>Descargar</a> </h2>";			
			?>
		</section>
			<?php			
				acf_form(array(
					'post_id'		=> get_the_ID(),
					'post_title'	=> false,
					'post_content'	=> false,
					'field_groups'       => array(106),
					'return'             => '%post_url%',
					'html_submit_button'	=> '<input type="submit" class="acf-button button btn button-primary button-large" value="%s" />',
					'html_submit_spinner'	=> '<span class="acf-spinner"></span>',
					'submit_value' => __("Actualizar Papeleria", 'acf'),
					'new_post'		=> array(
						'post_type'		=> 'papeleria',
						'post_status'	=> 'publish'
					)
				));
			?>
			<style>
			.acf-form-submit {
				display: block;
				text-align: right;
			}
			</style>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();