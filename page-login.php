<?php
/**
 * Template Name: Page Login
 * Plantilla para crear CV.
 *
 * @author Jonathan Rivera
 * @since 1.0.0
 */
if ( is_user_logged_in() ) { wp_redirect( home_url('/index.php/crear-mi-cv/') ); exit;}
get_header(); ?>
<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <h4 class="registration_link"><a href="<?php echo  home_url('/index.php/registrate/');?>"><?php _e('Nuevo Usuario?, Crea tu cuenta.'); ?></a></h4>
        <?php echo do_shortcode('[login_form]');?>
        
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();?>
