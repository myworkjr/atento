<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<div class="site-info">
	<a href="<?php echo esc_url( __( 'http://atento.creatucv.com/index.php/crear-mi-cv/', 'twentyseventeen' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentyseventeen' ), 'CreaTuCV.com' ); ?></a>
	<a href="<?php echo wp_logout_url( home_url() ); ?>"><?php  echo "Salir"; ?></a>

	
</div><!-- .site-info -->